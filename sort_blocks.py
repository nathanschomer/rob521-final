#!/usr/bin/env python3
# coding=utf8
import sys
sys.path.append('/home/pi/ArmPi/')
sys.path.append('./src')
import cv2
import time
from Camera import Camera
from LABConfig import *
from ArmIK.Transform import *
from ArmIK.ArmMoveIK import *
import HiwonderSDK.Board as Board
from CameraCalibration.CalibrationConfig import *
from VisionUtils import VisionUtils
from MoveUtils import MoveUtils
import signal
from argparse import ArgumentParser
from threading import Event, Thread
from Bus import Bus
from calibrate import calibrate
from pathlib import Path


_stop = Event()


def img_show(kill_thread, img_bus, color_bus):
    """
    Read image from img_bus and display
    """
    while not kill_thread.is_set():
        img = img_bus.read()
        if img is not None:

            # add text
            text = 'Dectected Color: {}'.format(color_bus.read())
            cv2.putText(img, text, (10, 35),
                        cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2)

            # display image
            cv2.imshow('ArmPi FPV', img)
            key = cv2.waitKey(1)


def vision(kill_thread, img_bus, block_delta, block_size, color_bus, gripper_cal, debug):

    my_camera = Camera()
    my_camera.camera_open()
    img_utils = VisionUtils()

    colors = ['red', 'blue', 'green']

    range_rgb = {
        'red': (0, 0, 255),
        'blue': (255, 0, 0),
        'green': (0, 255, 0),
    }

    while not kill_thread.is_set():
        img = my_camera.frame

        if img is not None:

            img = cv2.rotate(img, cv2.ROTATE_180)
            img_bus.write(img)

            # find location of red block
            frame_lab = img_utils.preprocess(img, get_roi=False, roi=None, start_pick_up=False)

            areaMaxContour = -1
            area_max = -1
            max_color = None

            # determine which color covers the largest area in this frame
            for color in colors:
                curr_areaMaxContour, curr_area_max = img_utils.detect_color_contour(frame_lab, color_range[color])
                if curr_area_max > area_max:
                    areaMaxContour = curr_areaMaxContour
                    area_max = curr_area_max
                    max_color = color

            # if red block is within frame
            if area_max > 2500:

                color_bus.write(max_color)

                rect = cv2.minAreaRect(areaMaxContour)
                box = np.int0(cv2.boxPoints(rect))
                img = cv2.drawContours(img, [box], -1, range_rgb[color], 2)

                # calc center point in frame and draw line
                block_center = img_utils.get_box_center(box)
                img = cv2.line(img, block_center, gripper_cal, (256, 0, 0), 2) 

                # calc delta
                delta = np.array(block_center) - np.array(gripper_cal)
                block_delta.write(delta)

                # calc percent of frame that is block
                total_img_area = np.prod(img.shape[:2])
                perc_block = area_max / total_img_area
                block_size.write(perc_block)

            else:
                block_size.write(0)

        else:
            block_size.write(0)


def move(kill_thread, img_bus, block_delta, block_size, color_bus, debug):

    x, y, z = 0, 0, 0

    move_utils = MoveUtils()

    # generate state encodings
    state_names = ['search', 'approach', 'grasp', 'delivery']
    states = {state : n for n, state in enumerate(state_names)}

    # set initial state
    state = states['search']

    move_utils.gripper_open()

    home_locs = {
        'red':   (-15 - 1.0, 12 - 0.5, 1.5),
        'green': (-15 - 1.0, 6 - 0.5,  1.5),
        'blue':  (-15 - 1.0, 0 - 0.5,  1.5),
    }

    while not kill_thread.is_set():

        if state == states['search']:

            thresh = 0.05
            x, y, z = move_utils.scan(block_size, thresh)

            if block_size.read() > thresh:
                state += 1 # move to next state

        elif state == states['approach']:

            if block_size.read() == 0:
                state -= 1
                continue

            sensitivity = 10
            step_size = 0.25
            x_step_size = 0.125
            size_thresh = 0.18

            dx, dy = block_delta.read()

            if dx > sensitivity:
                x += x_step_size
            elif dx < -1*sensitivity:
                x -= x_step_size

            if dy > sensitivity:
                z -= step_size
            elif dy < -1*sensitivity:
                y += step_size
            elif z > 1.5:
                z -= 0.125

            if abs(dx) < sensitivity and z <= 2 and block_size.read() > size_thresh:
                # move to next state
                state += 1
            else:
                move_utils.move_arm(x, y, z)
                #time.sleep(0.05)

        elif state == states['grasp']:
            move_utils.gripper_close()
            state += 1

        elif state == states['delivery']:
            # deposit color in sorting area
            # TODO: this is kinda shitty... should probably check 
            #   throughout this method that target color stays the same
            target_pos = home_locs[color_bus.read()]
            x, y, z = target_pos
            move_utils.move_arm(x, y, 7)
            move_utils.arm_lower_gentle(x, y, z)
            move_utils.gripper_open()
            move_utils.raise_arm()
            state = states['search']

    # move back to rest before shutting down
    move_utils.rest_position()
    move_utils.gripper_open()


def get_args():
    parser = ArgumentParser()
    parser.add_argument('--show-img', action='store_true',
                        help='inidcates camera view should be displayed')
    parser.add_argument('--no-move', action='store_true',
                        help='prevents movement thread from running')
    parser.add_argument('--no-vision', action='store_true',
                        help='prevents vision thread from running')
    parser.add_argument('--debug', action='store_true',
                        help='enable debug mode')
    parser.add_argument('--cal', action='store_true',
                        help='Run end effector/ camera alignment calibration')
    return parser.parse_args()


def sigint_handler(sig, frame):
    global _stop
    _stop.set()


def calibration(force_cal=False):

    cal_path = Path('./cfg/gripper_cal.npy')

    if force_cal or not cal_path.is_file():
        # close gripper
        move_utils = MoveUtils()
        move_utils.gripper_close()

        # run calibration
        cal = calibrate()

        # make cfg dir if necessary
        if not cal_path.parent.is_dir():
            cal_path.parent.mkdir()

        # save config to file
        np.save(cal_path, np.array(cal))
        move_utils.gripper_open()
    else:
        cal = tuple(np.load(cal_path, 'r'))


    return cal


if __name__ == "__main__":

    # set up sig handler
    signal.signal(signal.SIGINT, sigint_handler)

    args = get_args()

    gripper_cal = calibration(force_cal=args.cal)

    img_bus = Bus()
    block_delta = Bus()
    color_bus = Bus()
    block_size = Bus()
    block_size.write(0)

    vision_thread = None
    imshow_thread = None
    move_thread = None

    # spin up threads
    if not args.no_vision:
        vision_args = [_stop, img_bus, block_delta, block_size,
                       color_bus, gripper_cal, args.debug]
        vision_thread = Thread(target=vision, args=vision_args)
        vision_thread.start()
        
        if args.show_img:
            imshow_args = [_stop, img_bus, color_bus]
            imshow_thread = Thread(target=img_show, args=imshow_args)
            imshow_thread.start()

    if not args.no_move:
        move_args = [_stop, img_bus, block_delta,
                     block_size, color_bus, args.debug]
        move_thread = Thread(target=move, args=move_args)
        move_thread.start()

    # wait for threads to finish
    if vision_thread:
        vision_thread.join()
    if imshow_thread:
        imshow_thread.join()
    if move_thread:
        move_thread.join()
