import cv2
from camera import Camera

mouse_x = mouse_y = None

def calibrate():
    global mouse_x, mouse_y
    my_camera = Camera()
    my_camera.camera_open()
    #my_camera = cv2.VideoCapture(0, cv2.CAP_DSHOW)

    while True:
        # reading the image
        img = my_camera.frame
        img = cv2.rotate(img, cv2.ROTATE_180)
        #ret, img = my_camera.read()
   
        if img is not None:

            cv2.imshow('img', img)
        
            cv2.setMouseCallback('img', _click_event)
            if mouse_x is not None:
                #print((mouse_x, mouse_y))
                return (mouse_x, mouse_y)
        
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

    # close the window
    cv2.destroyAllWindows()

def _click_event(event, x, y, flags, params):
    global mouse_x, mouse_y
    if event == cv2.EVENT_LBUTTONDOWN:
        mouse_x = x
        mouse_y = y

if __name__=="__main__":
    p = calibrate()

    print(p)
