#!/usr/bin/env python3
# encoding:utf-8

import sys
sys.path.append('/home/pi/ArmPi/')
from ArmIK.Transform import *
from ArmIK.ArmMoveIK import *
import HiwonderSDK.Board as Board
import time
from Bus import Bus


class MoveUtils:

    def __init__(self):
        # The angle at which the gripper is closed when gripping
        self.gripper_close_ang = 500
        self.AK = ArmIK()

    def gripper_open(self):
        tmp = self.gripper_close_ang - 280
        Board.setBusServoPulse(1, tmp, 500)  # Paws open
        time.sleep(0.8)

    def gripper_close(self):
        Board.setBusServoPulse(1, self.gripper_close_ang, 500)  # Holder closed
        time.sleep(0.8)

    def gripper_angle(self, angle):
        Board.setBusServoPulse(2, angle, 500)
        time.sleep(0.5)

    def arm_raise(self, world_X, world_Y):
        Board.setBusServoPulse(2, 500, 500)
        self.AK.setPitchRangeMoving((world_X, world_Y, 12), -90, -90, 0, 1000)  # Robotic arm up
        time.sleep(0.8)

    def arm_lower_gentle(self, world_X, world_Y, world_Z):
        self.AK.setPitchRangeMoving((world_X, world_Y, world_Z + 3), -90, -90, 0, 500)
        time.sleep(0.5)
        self.AK.setPitchRangeMoving((world_X, world_Y, world_Z), -90, -90, 0, 1000)
        time.sleep(0.8)

    def arm_lower(self, world_X, world_Y):
        self.AK.setPitchRangeMoving((world_X, world_Y, 2), -90, -90, 0, 1000)  # lower the altitude
        time.sleep(0.8)

    def move_arm(self, world_X, world_Y, world_Z=12):
        result = self.AK.setPitchRangeMoving((world_X, world_Y, world_Z), -90, -90, 0)
        time.sleep(result[2]/1000)

    def initMove(self):
        Board.setBusServoPulse(1, self.gripper_close_ang - 50, 300)
        Board.setBusServoPulse(2, 500, 500)
        self.AK.setPitchRangeMoving((0, 10, 10), -30, -30, -90, 1500)
        time.sleep(1.5)

    def first_move(self, world_X, world_Y):
        result = self.AK.setPitchRangeMoving((world_X, world_Y - 2, 5), -90, -90, 0)
        if result == False:
            unreachable = True
        else:
            unreachable = False
        time.sleep(result[2]/1000) # The third item of the return parameter is time
        return unreachable

    def rest_position(self):
        self.AK.setPitchRangeMoving((0, 5, 15), -90, -90, 0, 1000)
        time.sleep(1)

    def raise_arm(self):
        self.AK.setPitchRangeMoving((0, 10, 12), -90, -90, 0, 1000)
        time.sleep(1)

    def scan(self, block_size, thresh):

        for x in range(-5, 5):
            self.AK.setPitchRangeMoving((x, 10, 13), -90, -90, 0, 1000)
            time.sleep(0.5)
            if block_size.read() > thresh:
                return (x, 10, 13)

        for x in range(5, -5, -1):
            self.AK.setPitchRangeMoving((x, 10, 9), -90, -90, 0, 1000)
            time.sleep(0.5)
            if block_size.read() > thresh:
                return (x, 10, 9)

        return (-4, 10, 9)

    def lower(self):
        self.AK.setPitchRangeMoving((1, 21, 1), -90, -90, 0, 1000)
