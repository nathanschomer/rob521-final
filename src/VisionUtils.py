#!/usr/bin/env python3
# encoding:utf-8

import cv2
import sys
sys.path.append('/home/pi/ArmPi/')
from LABConfig import *
from ArmIK.Transform import *
from ArmIK.ArmMoveIK import *
from LABConfig import *
import numpy as np
from camera import Camera


class VisionUtils():

    def __init__(self):
        return

    # Find the contour with the largest area
    # The parameter is a list of contours to be compared
    @staticmethod
    def getAreaMaxContour(contours):
        contour_area_temp = 0
        contour_area_max = 0
        area_max_contour = None

        for c in contours:  # Traverse all contours
            contour_area_temp = math.fabs(cv2.contourArea(c))  # Calculate the contour area
            if contour_area_temp > contour_area_max:
                contour_area_max = contour_area_temp
                if contour_area_temp > 300:  
                    area_max_contour = c

        return area_max_contour, contour_area_max  # Return the largest contour

    def draw_plus(self, img):
        img_h, img_w = img.shape[:2]
        cv2.line(img, (0, int(img_h / 2)), (img_w, int(img_h / 2)), (0, 0, 200), 1)
        cv2.line(img, (int(img_w / 2), 0), (int(img_w / 2), img_h), (0, 0, 200), 1)
        return img

    def preprocess(self, img, get_roi, roi, start_pick_up):

        size = (640, 480)

        frame_resize = cv2.resize(img, size, interpolation=cv2.INTER_NEAREST)
        frame_gb = cv2.GaussianBlur(frame_resize, (11, 11), 11)

        # If a recognized object is detected in a certain area，Then keep detecting the area until there is no
        # if get_roi and start_pick_up:
            # get_roi = False
            # frame_gb = getMaskROI(frame_gb, roi, size)    

        frame_lab = cv2.cvtColor(frame_gb, cv2.COLOR_BGR2LAB)  # Convert image to LAB space

        return frame_lab

    def detect_color_contour(self, frame_lab, color_range):

        frame_mask = cv2.inRange(frame_lab, color_range[0], color_range[1])  # Perform bit operations on the original image and mask

        # isolate detecteced "blobs" then find maximum region
        opened = cv2.morphologyEx(frame_mask, cv2.MORPH_OPEN, np.ones((6, 6), np.uint8))  # Open operation
        closed = cv2.morphologyEx(opened, cv2.MORPH_CLOSE, np.ones((6, 6), np.uint8))  # Closed operation
        contours = cv2.findContours(closed, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)[-2]  # Find the outline
        areaMaxContour, area_max = self.getAreaMaxContour(contours)  # Find the largest contour

        return areaMaxContour, area_max

    def label_img(self, img, box, world_x, world_y, color):
        cv2.drawContours(img, [box], -1, color, 2)
        cv2.putText(img, '(' + str(world_x) + ',' + str(world_y) + ')', (min(box[0, 0], box[2, 0]), box[2, 1] - 10),
                cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 1) #Draw center point
        return img

    def get_box_center(self, box):
        
        assert box.shape == (4, 2)

        x_min = box[:, 0].min()
        x_max = box[:, 0].max()
        y_min = box[:, 1].min()
        y_max = box[:, 1].max()

        x_cent = x_min + (x_max - x_min) // 2
        y_cent = y_min + (y_max - y_min) // 2

        return (x_cent, y_cent)


    def calc_block_area(self, box):

        assert box.shape == (4, 2)

        x_min = box[:, 0].min()
        x_max = box[:, 0].max()
        y_min = box[:, 1].min()
        y_max = box[:, 1].max()

        x = x_max - x_min
        y = y_max - y_min

        return x*y


if __name__ == "__main__":

    my_camera = Camera()
    my_camera.camera_open()
    img_utils = ImageUtils()

    # TODO: set calibration routine to set this value
    #   ...have user click location on image and save
    #       location to numpy file?
    gripper_center = (317, 358)

    while True:
        img = my_camera.frame

        if img is not None:

            img = cv2.rotate(img, cv2.ROTATE_180)

            # find location of red block
            frame_lab = img_utils.preprocess(img, get_roi=False, roi=None, start_pick_up=False)
            areaMaxContour, area_max = img_utils.detect_color_contour(frame_lab, color_range['red'])

            # if red block is within frame
            if area_max > 2500:
                rect = cv2.minAreaRect(areaMaxContour)
                box = np.int0(cv2.boxPoints(rect))
                img = cv2.drawContours(img, [box], -1, (0, 0, 255), 2)

                # calc center point in frame and draw line
                block_center = img_utils.get_box_center(box)
                img = cv2.line(img, block_center, gripper_center, (256, 0, 0), 2) 

                # calc delta
                delta = np.array(block_center) - np.array(gripper_center)

                # calc percent of frame that is block
                total_img_area = np.prod(img.shape[:2])
                perc_block = area_max / total_img_area

                print("delta = {}".format(delta))
                print("area = {}%\n".format(perc_block*100))

            cv2.imshow('img', img)
            key = cv2.waitKey(1)
            if key == 27:
                break

    my_camera.camera_close()
    cv2.destroyAllWindows()
