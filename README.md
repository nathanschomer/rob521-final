This project is a re-implementation of the ArmPi ColorSorting code re-designed for an arm with a first-person-view camera. The arm scans the search area for a red, green, and blue blocks. When one is detected, the arm approaches, grasps, and then delivers the block to the proper sorting area. After delivering a block to the sorting area, the arm will return to searching the search area.

![](images/arm.jpg)
